import DockerConfig from "@/model/DockerConfig";

export default interface IContainer {
    defaultName: string | null;
    id: string;
    generated: DockerConfig | null;
}
