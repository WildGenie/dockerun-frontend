import IImage from '@/interface/IImage';

export default class DockerConfig {

    private readonly _containerName: string;
    private readonly _image: IImage;
    private readonly _version: string;
    private readonly _envs: {[key: string]: string};
    private readonly _ports: {[key: string]: string};
    private readonly _volumes: {[key: string]: string};
    private readonly _links: {[key: string]: string};
    
    private readonly _restartPolicy: string;

    public constructor(containerName: string, image: IImage, version: string, envs: {[key: string]: string}, ports: {[key: string]: string}, volumes: {[key: string]: string}, links: {[key: string]: string}, restartPolicy: string) {
        this._containerName = DockerConfig.normalizeContainerName(containerName);
        this._image = image;
        this._version = version;
        this._envs = envs;
        this._ports = ports;
        this._volumes = volumes;
        this._links = links;
        
        this._restartPolicy = restartPolicy;
    }

    // Replaces whitespaces with underscores
    public static normalizeContainerName(containerName: string): string {
        return containerName.replace(/ /g,"_");
    }

    get containerName(): string {
        return this._containerName;
    }

    get image(): IImage {
        return this._image;
    }

    get version(): string {
        return this._version;
    }

    get envs(): {[key: string]: string} {
        return this._envs;
    }

    get ports(): {[key: string]: string} {
        return this._ports;
    }

    get volumes(): {[key: string]: string} {
        return this._volumes;
    }

    get links(): {[key: string]: string} {
        return this._links;
    }

    get restartPolicy(): string {
        return this._restartPolicy;
    }
}
