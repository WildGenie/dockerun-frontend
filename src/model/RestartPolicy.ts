export default class RestartPolicy {
    value: string;
    text: string;
    description: string;

    constructor(value: string, text: string, description: string) {
        this.value = value;
        this.text = text;
        this.description = description;
    }
}