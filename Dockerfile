FROM httpd:alpine

RUN apk update && apk upgrade && \
    apk add --no-cache --update nodejs npm

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i

COPY . .

RUN npm run build && \
    mv -v dist/* /usr/local/apache2/htdocs/ && \
    rm -r *
